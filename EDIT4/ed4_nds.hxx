
void ED4_create_NDS_awars(AW_root *root);
AW_window *ED4_create_nds_window(AW_root *root);

void ED4_get_NDS_sizes(int *width, int *brackets);
char *ED4_get_NDS_text(ED4_species_manager *species_man);
