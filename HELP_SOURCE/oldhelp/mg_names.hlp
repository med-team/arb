#Please insert up references in the next lines (line starts with keyword UP)
UP      arb.hlp
UP      glossary.hlp
UP      arb_merge_workflow.hlp

#Please insert subtopic references  (line starts with keyword SUB)
SUB     rename.hlp

# Hypertext links in helptext can be added like this: LINK{ref.hlp|http://add|bla@domain}

#************* Title of helpfile !! and start of real helpfile ********
TITLE           Synchronize species IDs before merging

DESCRIPTION     When merging two databases it is VERY IMPORTANT to use identical
                IDs ('name') for identical species and different IDs for different species.
                If not, data of different species will be merged or overwritten.

                Species are identified using the accession number and (optionally) one
                additional field. If you use an additional field, a different nameserver
                is used to synchronize species IDs.

                Because of different name servers, merging two databases is only possible
                if both databases use no additional field or if they use the same field.

                To synchronize IDs of all species in both databases, press the 'Check IDs ...' button
                to open the 'Synchronize IDs' window.

                Make sure both databases use the same additional id.

                Press the 'Synchronize' button to synchronize species IDs in both databases.


NOTES           Automatically creates unique IDs (=shortname) for all
                species entries in the database. The entries are identified by
                their accession numbers (public databases). The IDs are created
                using the 'full_name' information. Usually, the first three
                letters are taken from the genus designation, the remaining
                letters from the species name.

                In some cases where you are absolutely sure that both databases use
                identical IDs for identical species, but renaming creates
                different IDs (e.g. because one DB lacks accession numbers)
                you can override ID synchronization by checking the 'Override' toggle.

WARNINGS        If there are duplicated entries (same accession number -
                different 'full_name'; no accession number - same 'full_name')
                the different versions are indicated by appending running
                numbers separated from the 'name' by a dot.

                Normally merging is not allowed when there are duplicated species.

                You may override that behavior by checking 'Allow merging duplicates',
                but be warned:

                    It is VERY DANGEROUS to merge when duplicated species are in your
                    databases, cause there is no guarantee, that duplicates with the
                    same .NUM suffix refer to the same species.
                    You may easily overwrite or duplicate your data!

                Similar applies to the 'Override' toggle. Be careful!

BUGS            No bugs known
