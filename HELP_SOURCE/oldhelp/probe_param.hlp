#Please insert up references in the next lines (line starts with keyword UP)
UP	arb.hlp
UP	glossary.hlp
UP	pt_server.hlp
UP	probedesign.hlp

#Please insert subtopic references  (line starts with keyword SUB)
#SUB	pd_spec_param.hlp
SUB	probedesignresult.hlp

# Hypertext links in helptext can be added like this: LINK{ref.hlp|http://add|bla@domain}

#************* Title of helpfile !! and start of real helpfile ********
TITLE		Probe Parameters

OCCURRENCE	ARB_NT/ETC/Probe Design/PROBE DESIGN

DESCRIPTION	Allows to select the 'PT_SERVER' and its database, to customize
		the presentation of the results, define the stringency of target
		search and physical characteristics of the probes.

		Select a 'PT_SERVER':

			Select the appropriate 'PT_SERVER' from the menu
			displayed after pressing the button below the
			'PT_SERVER' prompt. The 'PT_SERVER' database (*.arb)
			has to be consistent with the current. (see 'Main
			Topics': 'Probe Design'; 'PT_SERVER What Why and How')

		Customize output of results:

			Type the number of probe target proposals to be
			displayed in the 'PD RESULT' window by typing it to the
			'Length of output' subwindow.

		Define number of accepted non-targets:

			Type the number of non-target species which you would
			accept to be detected by the probe to the 'Max. non
			group hits' subwindow.

			This helps not to miss potential target sites in case
			that species belonging to the particular specificity
			group had been overlooked while marking (see: 'Main
			Topics': 'Probe Design').

		Define number of accepted inter- and intraprobe base pairings:

			Type the number of potential base pairings within or
			between probe molecules.
			!!! Not yet implemented !!!

		Define minimum number of target species:

			Type the fraction (%) of marked species which have to
			share the target site to the 'Min. group hits (%)
			subwindow.

			This helps to design multiple probes in case that common
			target sites are not present in all species of the
			particular specificity group.

		Define length of the probe:

			Type minimum and maximum number of nucleotides to the
			corresponding 'Length of probe' subwindows to define
			probe length.

		Define dissociation temperature:

			A range of allowed dissociation temperatures (= 4xGC +
			2xAU; centigrade) can be defined by typing minimum and
			maximum values to the corresponding subwindows.

		Define the G+C content:

			A range of allowed G+C fractions (%) can be defined by
			typing minimum and maximum values to the corresponding
			subwindows.

		Define target region:

			A preferred sequence (alignment) region can be defined
			for the probe target sites by typing the nucleotide
			numbers of the homologous positions within the E. coli
			molecule.

			This requires:

				1. Your (pt_server) data is aligned.

				2. There is a SAI named 'ECOLI' which
				   includes the reference sequence.

		Press the 'EXPERT' button to display the 'PD SPECIAL' window if
			base pairings should be individually weighted (see LINK{pd_spec_param.hlp}).

		Press the 'GO' button to start the calculations.


NOTES		Increasing of the 'Max. non group hits' and reducing
		'Min. group hits (%)' values as well as increasing of the
		difference of the minimum and maximum values for 'Length of
		probe', 'Temperature' as well as 'G+C content' reduces the
		performance (speed) of the program.

		The results will be shown within the 'PD RESULT' window which
		can be displayed by pressing the 'RESULT' button. The window is
		automatically displayed when the probe search is completed.

EXAMPLES	None

WARNINGS	None

BUGS		No bugs known
