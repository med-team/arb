#Please insert up references in the next lines (line starts with keyword UP)
UP      arb.hlp
UP      glossary.hlp

#Please insert subtopic references  (line starts with keyword SUB)
SUB     rename.hlp

# Hypertext links in helptext can be added like this: LINK{ref.hlp|http://add|bla@domain}

#************* Title of helpfile !! and start of real helpfile ********
TITLE           Synchronize species IDs

OCCURRENCE      ARB_NT/Species/Synchronize IDs

DESCRIPTION     Automatically creates unique identifiers (=shortnames) for the
                species entries in the database. The entries are identified by
                their accession numbers (public databases).
                These IDs are created using information from the 'full_name'.
                Usually, the first three letters are taken from the genus designation,
                the remaining letters from the species name.

                If there are duplicated entries (same accession number -
                different 'full_name'; no accession number - same 'full_name')
                the different versions are indicated by appending running
                numbers separated from the 'name' by a dot.

                Select the 'Synchronize IDs' entry of the 'Species' menu to display
                the 'Synchronize species IDs' window.

NOTES           The IDs are stored with the database. They are protected versus change
                to avoid assigning the same ID to different species.

                It is recommended to assign accession numbers (field 'acc') to species
                entries (sequences) which are not from public databases and therefore
                dont have an accession number yet. This is done automatically when
                importing new sequences.

EXAMPLES        None

WARNINGS        None

BUGS            No bugs known
