# ----------------------------------------------------------
#   _  _  __ _  __  ____    ____  ____  ____  ____  ____
#  / )( \(  ( \(  )(_  _)  (_  _)(  __)/ ___)(_  _)/ ___)
#  ) \/ (/    / )(   )(      )(   ) _) \___ \  )(  \___ \
#  \____/\_)__)(__) (__)    (__) (____)(____/ (__) (____/
#
# ----------------------------------------------------------


.SUFFIXES: .cxx .o .sym

dummy:
	false

include Makefile.setup.include


RELUNITDIR=../$(UNITDIR)
FULLUNITDIR=$(ARBHOME)/$(UNITDIR)

SHAREDLIBS=\
	CORE \
	ARBDB \
	AWT \
	WINDOW \

EXEOBJDIRS=\
	TOOLS \
	PERLTOOLS \
	HELP_SOURCE \
	AISC_MKPTPS \

ISSHAREDLIB=$(findstring $(UNITDIR),$(SHAREDLIBS))
ISEXEOBJECT=$(findstring $(UNITDIR),$(EXEOBJDIRS))

ifneq ($(ISSHAREDLIB),)
	UNITLIB=$(UNITLIBNAME).$(SHARED_LIB_SUFFIX)
else
ifneq ($(ISEXEOBJECT),)
	UNITLIB=$(UNITLIBNAME).o
else
	UNITLIB=$(UNITLIBNAME).a
endif
endif

UNIQUE_NAME=test_$(subst /,_,$(subst .,_,$(UNITDIR)_$(UNITLIB)))

ifneq ($(ISSHAREDLIB),)
FULLLIB=$(ARBHOME)/lib/$(UNITLIB)
else
FULLLIB=$(FULLUNITDIR)/$(UNITLIB)
endif

DESTDIR=tests
RUNDIR=run# see also UnitTester.cxx@chdir

SYMLIST=$(DESTDIR)/$(UNIQUE_NAME).sym
TEST_CODE=$(DESTDIR)/$(UNIQUE_NAME).cxx
TEST_OBJ=$(DESTDIR)/$(UNIQUE_NAME).o
TEST_EXE=$(DESTDIR)/$(UNIQUE_NAME)

MAKESYMLIST=./make_symlist.sh
SYM2TESTCODE=./sym2testcode.pl

INCDIR=../INCLUDE

INCLUDES= \
	UnitTester.hxx \
	$(INCDIR)/arb_assert.h \

UNIT_TESTER_LIB=./UNIT_TESTER.a

ifeq ($(UNITLIB),.a)
# nothing given (only avoids undefined symbols)
LINKLIST=
LINKDEPS=
else
LINKLIST=$(shell ../SOURCE_TOOLS/needed_libs.pl -S -A .. -l $(FULLLIB))
LINKDEPS=$(shell ../SOURCE_TOOLS/needed_libs.pl -S -A .. -F $(FULLLIB))
endif

ifeq ($(CHECK_LEAKS),0)
	LEAKS:=
else
ifeq ($(CHECK_LEAKS),1)
	LEAKS:=-l
else
	LEAKS:=-l -r
endif
endif

# --------------------------------------------------------------------------------

clean:
	rm -f $(DESTDIR)/*

# --------------------------------------------------------------------------------

$(DESTDIR):
	test -d $(DESTDIR) || mkdir -p $(DESTDIR)

$(RUNDIR):
	test -d $(RUNDIR) || mkdir -p $(RUNDIR)

makedirs: $(DESTDIR) $(RUNDIR)

$(SYMLIST) : $(FULLLIB) Makefile makedirs show_test_header $(MAKESYMLIST)
	$(MAKESYMLIST) $(FULLLIB) $(SYMLIST)

$(TEST_CODE) : $(SYMLIST) $(SYM2TESTCODE) Makefile.test
	$(SYM2TESTCODE) $(UNITLIB) $(RESTRICT_MODULE) $(RESTRICT_FUN) $< $@ $(WARN_LEVEL)

$(TEST_OBJ) : $(TEST_CODE) $(INCLUDES)
	$(A_CXX) $(cflags) $(cxxflags) -c $< -o $@ -I. $(POST_COMPILE) 

$(TEST_EXE) : $(TEST_OBJ) $(UNIT_TESTER_LIB) $(LINKDEPS)
	$(LINK_EXECUTABLE) $@ $< $(UNIT_TESTER_LIB) -L../lib $(LINKLIST) $(EXECLIBS)

dump:
	@echo "$(SEP) dump $(UNITLIBNAME)"
	@echo "UNITDIR        ='$(UNITDIR)'"
	@echo "UNITLIBNAME    ='$(UNITLIBNAME)'"
	@echo "FULLUNITDIR    ='$(FULLUNITDIR)'"
	@echo "ISSHAREDLIB    ='$(ISSHAREDLIB)'"
	@echo "UNITLIB        ='$(UNITLIB)'"
	@echo "FULLLIB        ='$(FULLLIB)'"
	@echo "UNIQUE_NAME    ='$(UNIQUE_NAME)'"
	@echo "SYMLIST        ='$(SYMLIST)'"
	@echo "TEST_CODE      ='$(TEST_CODE)'"
	@echo "TEST_OBJ       ='$(TEST_OBJ)'"
	@echo "TEST_EXE       ='$(TEST_EXE)'"
	@echo "INCLUDES       ='$(INCLUDES)'"
	@echo "LINKLIST       ='$(LINKLIST)'"
	@echo "LINKDEPS       ='$(LINKDEPS)'"

do_valgrind:
	@echo "Valgrinding $(TEST_EXE)"
	$(ARBHOME)/SOURCE_TOOLS/arb_valgrind -e -q $(LEAKS) -c 40 $(TEST_EXE) || \
		echo "UnitTester: $(UNITLIBNAME) valgrind reports error(s)"

show_test_header:
	@echo ""
	@echo "-------------------- [ Test $(UNITLIBNAME) ] --------------------"

perform_test: $(TEST_EXE)
	@echo "fake[2]: Entering directory \`$(FULLUNITDIR)'"
ifeq ($(findstring B,$(VALGRIND)),B)
	$(MAKE) -f Makefile.test do_valgrind
endif
	$(TEST_EXE) 
ifeq ($(COVERAGE),1)
	@echo "UnitTester: coverage" # do not remove (used as trigger)
	@echo "-------------------- test-coverage for $(UNITLIBNAME)"
	./gcov2msg.sh $(FULLUNITDIR) $(UNITLIBNAME)
	@echo "-------------------- test-coverage for $(UNITLIBNAME) [end]"
endif
ifeq ($(findstring A,$(VALGRIND)),A)
	$(MAKE) -f Makefile.test do_valgrind
endif
	@echo "fake[2]: Leaving directory \`$(FULLUNITDIR)'"

skip_test:
	@echo "UnitTester: $(UNITLIBNAME) tests=0 (skipped via RESTRICT_LIB)"

# --------------------

RUN:=perform_test

ifneq ('$(RESTRICT_LIB)','')
IS_RESTRICTED_TO=$(findstring :$(UNITLIBNAME):,:$(RESTRICT_LIB):)
ifeq ('$(IS_RESTRICTED_TO)','')
	RUN:=skip_test
endif
endif

runtest: $(RUN)

#runtest: dump

