Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Arb
Upstream-Contact: Arb development team <devel@arb-home.de>,
                  Ralf Westram <ralf@arb-home.de>
Source: http://download.arb-home.de/release/
Disclaimer: This package is not part of the Debian distribution due to its
 non-free license, which forbids being sold or incorporated into a commercial
 product.

Files: *
Copyright: 1994-2011
 Department of Microbiology:
  - Dr. Wolfgang Ludwig (Biologist) <ludwig@arb-home.de>
    Task: Group Leader; Inventor of ARB, knows everything about it
  - Ralf Westram (Computer scientist) <ralf@arb-home.de>
    Task: General ARB development
  - Dr. Lothar Richter (Biologist) <richter@arb-home.de>
    Task: ARB Database Curation and Automatization
  - Arno Buchner (Biologist) <buchner@arb-home.de>
    Task: ARB Database Curation and Genomics
 .
 Department of Computer Science:
  - Prof. Dr. Thomas Ludwig <thomas.ludwig@informatik.uni-heidelberg.de>
    Task:  Computer Science Supervisor
  - Dr. Harald Meier (Biologist) <meierh@arb-home.de>
    Task:  ARB GENOME programming
  - Gangolf Jobb (Biologist) <jobb@arb-home.de>
    Task:  Development and integration of alignment- and tree-generation-software
License: ARB
  * The ARB software and documentation are not in the public domain.
  * External programs distributed together with ARB are copyrighted
    by and are the property of their respective authors unless otherwise stated.
  * All other copyrights are owned by Lehrstuhl fuer Mikrobiologie, TU Muenchen.
 .
 USAGE LICENSE
 .
 You have the right to use this version of ARB for free. Please read as well the
 attached copyright notices below whether you may or may not install this package.
 .
 REDISTRIBUTION LICENSE
 .
  * This release of the ARB program and documentation may not be sold or
    incorporated into a commercial product, in whole or in part, without
    the expressed written consent of the Technical University of Munich and
    of its supervisors Oliver Strunk, Ralf Westram & Wolfgang Ludwig.
  * All interested parties may redistribute and modify ARB as long as all
    copies are accompanied by this license information and all copyright
    notices remain intact. Parties redistributing ARB must do so on a non-profit
    basis, charging only for cost of media.
  * If you modify parts of ARB and redistribute these changes the
    'Lehrstuhl fuer Mikrobiologie' gains the right to incorporate these changes
    into ARB and to redistribute them with future versions of ARB.
 .
 Disclaimer
 .
 THE TU MUENCHEN AND THE VARIOUS AUTHORS OF ARB GIVE NO WARRANTIES, EXPRESSED
 OR IMPLIED FOR THE SOFTWARE AND DOCUMENTATION PROVIDED, INCLUDING, BUT NOT
 LIMITED TO WARRANTY OF MERCHANTABILITY AND WARRANTY OF FITNESS FOR A PARTICULAR
 PURPOSE.
 User understands the software is a research tool for which no warranties as to
 capabilities or accuracy are made, and user accepts the software "as is."
 User assumes the entire risk as to the results and performance of the software
 and documentation. The above parties cannot be held liable for any direct,
 indirect, consequential or incidental damages with respect to any claim by user
 or any third party on account of, or arising from the use of software and
 associated materials. This disclaimer covers both the ARB core applications and
 all external programs used by ARB.
 .
 Additional not for Debian packaging:
 .
 Hereby anybody is granted the right to build debian-pakets of the ARB
 software package (http:://www.arb-home.de/) and publish them on debian
 mirrors (or any other way of debian-distribution).
 .
 This includes any debian derivates like ubuntu.
 .
 The ARB developers may (but most likely wont ever) revoke this
 granting. If really done so, it'll only affect ARB versions released
 after such a revocation.
 .
 9. July 2008
 Ralf Westram <westram@arb-home.de>

Files: GDE/*
Copyright: 2004-2008 The Board of Trustees of the University of Illinois and by Steven Smith
License: GDE
  * The Genetic Data Environment (GDE) software and documentation are not in the
    public domain. Portions of this code are owned and copyrighted by the
    The Board of Trustees of the University of Illinois and by Steven Smith.
    External functions used by GDE are the property of their authors.
    This release of the GDE program and documentation may not be sold, or
    incorporated into a commercial product, in whole or in part without the
    expressed written consent of the University of Illinois and of its author,
    Steven Smith.
  * All interested parties may redistribute the GDE as long as all copies are
    accompanied by this documentation, and all copyright notices remain intact.
    Parties interested in redistribution must do so on a non-profit basis,
    charging only for cost of media. Modifications to the GDE core editor
    should be forwarded to the author Steven Smith. External programs used
    by the GDE are copyrighted by, and are the property of their respective
    authors unless otherwise stated.

Files: GDE/PHYLIP/*
Copyright: (c) Copyright 1986-1993 by Joseph Felsenstein and the University
  of Washington.
License: PHYLIP_non-free
  Permission is granted to copy this document provided that
  no fee is charged for it and that this copyright notice is not removed.
Comment: Remark from the Debian package maintainer Andreas Tille
 Phylp is not used in the binary package because the Debian
 package is used which is in the non-free part of Debian.
 Remark: After heavy discussion with the author of Phylip it
 was not possible to convince him to use a free license.

Files: GDE/SUPPORT/CAP2.c
Copyright: (C) 1991   Xiaoqiu Huang
License: CAP2_non-free
 The distribution of the program is granted provided no charge
 is made and the copyright notice is included.
Comment: Remark from the Debian package maintainer Andreas Tille
 I was unable to find a source URL where this program which is
 claimed as external can be downloaded.

Files: CONVERTALN/*
Copyright: (C) 1992 Wen-Min Kuan
License: ARP_permissive
 The files in this directory do not contain an explicite license
 statement.  It has to be assumed that the license is the same as the
 general usage and redistribution license for Arb and the authors of
 Arb got a permission to distribute this code in their product.
 .
 This paragraph was just added to the copyright file to mention that
 there is some copyrighted work which was not mentioned above.
Comment: convert_aln --  an alignment(or sequence) converter written
 by Wen-Min Kuan for the Ribsomal Database Project(RDP), April 28, 1992.
 .
 Remark from the Debian package maintainer Andreas Tille:
 I was unable to find a source URL where this program which is
 claimed as external can be downloaded.

Files: GDE/FASTDNAML/*
Copyright: (C) 1998, 1999, 2000 by Gary J. Olsen,
 1986 - 1990 by the University of Washington and Joseph Felsenstein.
License: GPL
Comment: Remark from the Debian package maintainer Andreas Tille:
 This is not used in the binary package because it is available as
 official Debian package and has a free license (see this package).

Files: GDE/RAxML/a*
Copyright: (C) 2004-2008 Alexandros Stamatakis, Michael Ott, Thomas Ludwig
License: GPL
Comment: There is also a file GDE/RAxML/softwaredisclaimer in this directory
 which contains some license text which might be provided in a private
 mail or whatever from the RAxML authors to the Arb authors.  The relevance
 of this file is completely unclear because RAxML as it can be obtained from
 http://icwww.epfl.ch/~stamatak/index-Dateien/Page443.htm is GPLed and the
 remaining files in this directory are neither copyrighted by the RAxML
 authors nor do they feature a similar license statement.  This file is
 subject for clarification by the Arb authors.

Files: GDE/RAxML/c* GDE/RAxML/p*
Copyright: (C) 1993-2002 by the University of Washington.
 Written by Joseph Felsenstein, Hisashi Horino, and others
License: RAxML_non-free
 Permission is granted to copy and use this program provided no
 fee is charged for it and provided that this copyright notice is not
 removed.

Files: GDE/RAxML/g*
Copyright: (C) 1987, 1993, 1994
 The Regents of the University of California.  All rights reserved.
License: BSD-3-clause
 Copyright (c) The Regents of the University of California.
 All rights reserved.
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. Neither the name of the University nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.

Files: GDE/TREEPUZZLE/*
Copyright: (C) 1995-1999 Korbinian Strimmer and Arndt von Haeseler
 (C) 1999-2001 Heiko A. Schmidt, Korbinian Strimmer,Martin Vingron,
           Arndt von Haeseler
License: GPL
Comment: This code is not used in the binary package because it is available
 as official Debian package and has a free license (see this package).

Files: READSEQ/*
Copyright: 1990-1993 D. Gilbert <gilbertd@bio.indiana.edu>
License: PD
 readseq is public domain software
Comment: This code is not used in the binary package because it is
 available as official Debian package and has a free license (see this
 package).

Files: debian/*
Copyright: (C) 2008-2013 Andreas Tille <tille@debian.org>
License: GPL

License: GPL
 On Debian systems, the complete text of the GNU General Public
 License can be found in `/usr/share/common-licenses/GPL-2'.

