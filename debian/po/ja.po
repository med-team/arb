# Copyright (C) 2009 Debian-Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
# This file is distributed under the same license as the tipcutils package.
# Hideki Yamane (Debian-JP) <henrich@debian.or.jp>, 2009.
#
msgid ""
msgstr ""
"Project-Id-Version: arb 6.0.2\n"
"Report-Msgid-Bugs-To: arb@packages.debian.org\n"
"POT-Creation-Date: 2014-10-11 07:42+0200\n"
"PO-Revision-Date: 2014-10-11 14:42+0900\n"
"Last-Translator: Hideki Yamane (Debian-JP) <henrich@debian.or.jp>\n"
"Language-Team: Japanese <debian-japanese@lists.debian.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: multiselect
#. Description
#: ../arb-common.templates:2001
msgid "ARB PT-server administrators:"
msgstr "ARB PT サーバ管理者:"

#. Type: multiselect
#. Description
#: ../arb-common.templates:2001
msgid ""
"The default configuration of PT-server slots in /etc/arb/arb_tcp.dat gives "
"ARB three global slots accessible by all users (connecting to localhost:"
"${PORT}), as well as three slots to give private per-user access (connecting "
"to ~/.arb_pts/${USER}${NUMBER}.socket)."
msgstr ""
"/etc/arb/arb_tcp.dat にあるPTサーバスロットのデフォルト設定では ARB に全ユー"
"ザからアクセスできる3つのグローバルスロット (localhost:${PORT} に接続) と、"
"ユーザごとの個別のアクセスを受け持つ3つスロット (~/.arb_pts/${USER}${NUMBER}."
"socket に接続) を提供します。"

#. Type: multiselect
#. Description
#: ../arb-common.templates:2001
msgid ""
"Only members of the \"arb\" system group will be able to build and update "
"the shared PT-servers. Please enter the login names for these privileged "
"users."
msgstr ""
"システムグループ「arb」のメンバーだけが共有PTサーバをビルド、更新できます。権"
"限のあるユーザのログイン名を入力してください。"
